package com.test.demo.entity;

import io.searchbox.annotations.JestId;

public class Person {
    @JestId
    private Long id;
    private String name;
    private Integer age;

    public Person(Long id, String name, Integer age){
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
