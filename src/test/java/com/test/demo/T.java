package com.test.demo;

import com.test.demo.entity.Person;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.*;
import io.searchbox.indices.CreateIndex;
import io.searchbox.indices.DeleteIndex;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
public class T {
    @Autowired
    private JestClient jestClient;

    @Test
    public void createIndex() {
        try {
            JestResult jestResult = jestClient.execute(new CreateIndex.Builder("test_index").build());
            System.out.println("createIndex:{}" + jestResult.isSucceeded());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteIndex() {
        try {
            JestResult jestResult = jestClient.execute(new DeleteIndex.Builder("test_index").build());
            System.out.println("createIndex:{}" + jestResult.isSucceeded());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void a() throws Exception {

        List<Person> list = new ArrayList();
        Person p1 = new Person(1L, "zhangsan", 3);
        Person p2 = new Person(2L, "lisi", 4);
        Person p3 = new Person(3L, "zhangsan", 4);
        list.add(p1);
        list.add(p2);
        list.add(p3);

        Bulk.Builder bulk = new Bulk.Builder();
        for (Person entity : list) {
            Index index = new Index.Builder(entity).index("test_index").type("default").build();
            bulk.addAction(index);
        }
        BulkResult dr = jestClient.execute(bulk.build());
        if (!dr.isSucceeded()) {
            System.out.println("保存或更新索引文档失败->fail");
        }
        System.out.println("保存或更新索引文档成功->ok");
    }

    @Test
    public void t1() {
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.filter(QueryBuilders.termQuery("name", "zhangsan"));

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(boolQueryBuilder).size(0);

        System.out.println("DSL+" + searchSourceBuilder.toString());

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex("test_index").build();
        SearchResult searchResult = null;
        try {
            searchResult = jestClient.execute(search);
        } catch (IOException e) {
        }
        System.out.println(searchResult);
    }

    @Test
    public void t2() {
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        //过滤
//        boolQueryBuilder.filter(QueryBuilders.termQuery("name", "zhangsan"));


        AggregationBuilder aggregationBuilder =

                AggregationBuilders.terms("group_name").field("name").subAggregation(AggregationBuilders.sum("sumAge").field("age"));
        //限定es版本为2.4.6
        //springboot 2.1.x后，这里的order会使用key，而不是term。所以只能用2.1.x之前的
        ((TermsAggregationBuilder) aggregationBuilder).order();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(boolQueryBuilder).aggregation(aggregationBuilder).size(0);

        System.out.println("DSL+" + searchSourceBuilder.toString());

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex("test_index").build();
        SearchResult searchResult = null;
        try {
            searchResult = jestClient.execute(search);
        } catch (IOException e) {
        }
        System.out.println(searchResult);
    }

    @Test
    public void t3() {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder
                //排序
                .sort("id", SortOrder.ASC)
                //分页，从第几开始(第一个下标为0)，每页多少
                .from(0)
                .size(2);
        System.out.println("DSL+" + searchSourceBuilder.toString());

        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex("test_index").build();
        SearchResult searchResult = null;
        try {
            searchResult = jestClient.execute(search);
        } catch (IOException e) {
        }
        System.out.println(searchResult);
    }

}
